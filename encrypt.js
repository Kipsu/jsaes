const jwt = require('jsonwebtoken');
const jwtSecret = 'hilton-hotel-messaging';
const aes256 = require('aes256');
const aesKey = 'messaging-hotel-hilton';

var body = {
  "openId": "123",
  "ctyhocn": "MIABSDT",
  "hhonorsNumber": "8238191478|MIABSDT",
  "firstName": "Tommy",
  "lastName": "Boy",
  "honorific": "Mr.",
  "phoneNumber": null,
  "workstationId": "workstation1",
  "hotelName": "注册注册登录登录",
  "actions": [
    {"type": "button", "label": "注册", "path": "/first/path", "style": "primary"},
    // {"type": "link", "label": "The Googs", "path": "https://www.google.com", "params": "z=b"},
    {"label": "登录", "path": "/other/path", "params": "a=b"}
  ]
};
var payload = JSON.stringify(body);
var encrypted = aes256.encrypt(aesKey, payload);
var token = jwt.sign({'data': encrypted}, jwtSecret);

console.log('JWT: ', token);

